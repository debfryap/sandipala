<?php $page='dealers';?>
<?php include('inc_header.php');?>
  <div id="middle"class="bg_grey">
    <div class="wrapper">
      <div id="submenu"style="min-width:150px;"> <a href="#"class="active">Dealer Locator</a></div>
      <img src="images/content/about_banner.jpg"width="960"height="338"/>
      <div id="main_content">
        <div class="main_column"style="width:100%; float:none; background-image:none;">
          <div class="text">
            <h3>Dealer</h3>
            <ul id="dealer_nav">
              <li><a href="#"class="active">All Dealer</a> </li>
              <li><a href="#">Sales</a> </li>
              <li><a href="#">Services</a> </li>
              <li><a href="#">Spareparts</a></li>
            </ul>
            <div style="position:relative; width:920px;">
            <div id="white_box">
            
            </div>              
              <form action=""method="get"class="dealer_search">
                <span>Location  : </span>
                <label>Province</label>
                <select name="province">
                  <option value="Show All">Show Alsl</option>
                </select>
                <label>City</label>
                <select name="city">
                 
                                    <option class="row Nanggroe_Aceh_Darussalam" value="19" style="display: none;">Kota Banda Aceh</option>
                                    <option class="row Sumatera_Utara" value="33" style="display: none;">Kabupaten Langkat</option>
                                    <option class="row Sumatera_Utara" value="51" style="display: none;">Kota Medan</option>
                                    <option class="row Sumatera_Utara" value="52" style="display: none;">Kota Padangsidempuan</option>
                                    <option class="row Sumatera_Barat" value="70" style="display: none;">Kota Padang</option>
                                    <option class="row Sumatera_Barat" value="75" style="display: none;">Kota Solok</option>
                                    <option class="row Sumatera_Barat" value="87" style="display: none;">Kota Pekanbaru</option>
                                    <option class="row Kepulauan_Riau" value="93" style="display: block;">Kota Batam</option>
                                    <option class="row Kepulauan_Bangka-Belitung" value="101" style="display: block;">Kota Pangkal Pinang</option>
                                    <option class="row Jambi" value="104" style="display: none;">Kabupaten Sarolangun</option>
                                    <option class="row Jambi" value="111" style="display: none;">Kota Jambi</option>
                                    <option class="row Bengkulu" value="119" style="display: none;">Kabupaten Mukomuko</option>
                                    <option class="row Bengkulu" value="122" style="display: none;">Kota Bengkulu</option>
                                    <option class="row Sumatera_Selatan" value="125" style="display: none;">Kabupaten Lahat</option>
                                    <option class="row Sumatera_Selatan" value="136" style="display: none;">Kota Palembang</option>
                                    <option class="row Sumatera_Selatan" value="137" style="display: none;">Kota Prabumulih</option>
                                    <option class="row Lampung" value="145" style="display: none;">Kabupaten Pringsewu</option>
                                    <option class="row Lampung" value="147" style="display: none;">Kabupaten Tulang Bawang</option>
                                    <option class="row Lampung" value="150" style="display: none;">Kota Bandar Lampung</option>
                                    <option class="row Banten" value="156" style="display: none;">Kota Cilegon</option>
                                    <option class="row Banten" value="158" style="display: none;">Kota Tangerang</option>
                                    <option class="row DKI_Jakarta" value="161" style="display: none;">Kota Jakarta Barat</option>
                                    <option class="row DKI_Jakarta" value="162" style="display: none;">Kota Jakarta Pusat</option>
                                    <option class="row DKI_Jakarta" value="163" style="display: none;">Kota Jakarta Selatan</option>
                                    <option class="row DKI_Jakarta" value="164" style="display: none;">Kota Jakarta Timur</option>
                                    <option class="row DKI_Jakarta" value="165" style="display: none;">Kota Jakarta Utara</option>
                                    <option class="row Jawa_Barat" value="171" style="display: none;">Kabupaten Cianjur</option>
                                    <option class="row Jawa_Barat" value="173" style="display: none;">Kabupaten Garut</option>
                                    <option class="row Jawa_Barat" value="175" style="display: none;">Kabupaten Karawang</option>
                                    <option class="row Jawa_Barat" value="183" style="display: none;">Kota Bandung</option>
                                    <option class="row Jawa_Barat" value="185" style="display: none;">Kota Bekasi</option>
                                    <option class="row Jawa_Barat" value="186" style="display: none;">Kota Bogor</option>
                                    <option class="row Jawa_Barat" value="188" style="display: none;">Kota Cirebon</option>
                                    <option class="row Jawa_Barat" value="189" style="display: none;">Kota Depok</option>
                                    <option class="row Jawa_Barat" value="190" style="display: none;">Kota Sukabumi</option>
                                    <option class="row Jawa_Barat" value="191" style="display: none;">Kota Tasikmalaya</option>
                                    <option class="row Jawa_Tengah" value="198" style="display: none;">Kabupaten Cilacap</option>
                                    <option class="row Jawa_Tengah" value="205" style="display: none;">Kabupaten Klaten</option>
                                    <option class="row Jawa_Tengah" value="207" style="display: none;">Kabupaten Kudus</option>
                                    <option class="row Jawa_Tengah" value="221" style="display: none;">Kota Magelang</option>
                                    <option class="row Jawa_Tengah" value="222" style="display: none;">Kota Pekalongan</option>
                                    <option class="row Jawa_Tengah" value="224" style="display: none;">Kota Semarang</option>
                                    <option class="row Jawa_Tengah" value="226" style="display: none;">Kota Tegal</option>
                                    <option class="row Daerah_Istimewa_Yogyakarta" value="231" style="display: none;">Kota Yogyakarta</option>
                                    <option class="row Jawa_Timur" value="233" style="display: none;">Kabupaten Banyuwangi</option>
                                    <option class="row Jawa_Timur" value="238" style="display: none;">Kabupaten Jember</option>
                                    <option class="row Jawa_Timur" value="255" style="display: none;">Kabupaten Sidoarjo</option>
                                    <option class="row Jawa_Timur" value="259" style="display: none;">Kabupaten Tuban</option>
                                    <option class="row Jawa_Timur" value="260" style="display: none;">Kabupaten Tulungagung</option>
                                    <option class="row Jawa_Timur" value="262" style="display: none;">Kota Blitar</option>
                                    <option class="row Jawa_Timur" value="263" style="display: none;">Kota Kediri</option>
                                    <option class="row Jawa_Timur" value="265" style="display: none;">Kota Malang</option>
                                    <option class="row Jawa_Timur" value="269" style="display: none;">Kota Surabaya</option>
                                    <option class="row Bali" value="278" style="display: none;">Kota Denpasar</option>
                                    <option class="row Nusa_Tenggara_Barat" value="288" style="display: none;">Kota Mataram</option>
                                    <option class="row Nusa_Tenggara_Timur" value="291" style="display: none;">Kabupaten Ende</option>
                                    <option class="row Nusa_Tenggara_Timur" value="309" style="display: none;">Kota Kupang</option>
                                    <option class="row Kalimantan_Barat" value="313" style="display: none;">Kabupaten Ketapang</option>
                                    <option class="row Kalimantan_Barat" value="321" style="display: none;">Kabupaten Sintang</option>
                                    <option class="row Kalimantan_Barat" value="322" style="display: none;">Kota Pontianak</option>
                                    <option class="row Kalimantan_Barat" value="323" style="display: none;">Kota Singkawang</option>
                                    <option class="row Kalimantan_Tengah" value="337" style="display: none;">Kota Palangkaraya</option>
                                    <option class="row Kalimantan_Selatan" value="350" style="display: none;">Kota Banjarmasin</option>
                                    <option class="row Kalimantan_Timur" value="351" style="display: none;">Kabupaten Berau</option>
                                    <option class="row Kalimantan_Timur" value="361" style="display: none;">Kota Balikpapan</option>
                                    <option class="row Kalimantan_Timur" value="363" style="display: none;">Kota Samarinda</option>
                                    <option class="row Gorontalo" value="370" style="display: none;">Kota Gorontalo</option>
                                    <option class="row Sulawesi_Selatan" value="376" style="display: none;">Kabupaten Gowa</option>
                                    <option class="row Sulawesi_Selatan" value="406" style="display: none;">Kota Kendari</option>
                                    <option class="row Sulawesi_Tenggara" value="417" style="display: none;">Kota Palu</option>
                                    <option class="row Sulawesi_Utara" value="430" style="display: none;">Kota Kotamobagu</option>
                                    <option class="row Sulawesi_Utara" value="431" style="display: none;">Kota Manado</option>
                                    <option class="row Sulawesi_Barat" value="435" style="display: none;">Kabupaten Mamuju</option>
                                    <option class="row Maluku" value="447" style="display: none;">Kota Ambon</option>
                                    <option class="row Maluku_Utara" value="456" style="display: none;">Kota Ternate</option>
                                    <option class="row Papua_Barat" value="460" style="display: none;">Kabupaten Manokwari</option>
                                    <option class="row Papua_Barat" value="468" style="display: none;">Kota Sorong</option>
                                    <option class="row Papua" value="497" style="display: none;">Kota Jayapura</option>
                </select>
                <input name="search"type="submit"value="Submit"/>
              </form>
              <iframe width="895"height="350"frameborder="0"scrolling="no"marginheight="0"marginwidth="0"src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;q=-0.789275,113.921327&amp;aq=&amp;sll=-0.789275,113.921327&amp;sspn=37.913322,67.631836&amp;ie=UTF8&amp;hnear=Indonesia&amp;t=m&amp;rq=1&amp;split=0&amp;ll=-0.789275,113.921327&amp;spn=37.913322,67.631836&amp;z=5&amp;output=embed"style="margin-top:20px; margin-left:10px; border:solid 1px #c3c4c4;"></iframe>
              <ul id="dealer_result">
                <li  data-id="kemayoran"><img src="images/content/dealer.jpg"width="199"height="140"class="img"/> <span><img src="images/material/hino.png"width="57"height="13"/> Kemayoran</span></li>
                <li data-id="tomang"><img src="images/content/dealer-08.jpg"width="198"height="139"class="img"/> <span><img src="images/material/hino.png"width="57"height="13"/> Tomang</span></li>
                <li><img src="images/content/dealer-12.jpg"width="198"height="140"class="img"/> <span><img src="images/material/hino.png"width="57"height="13"/> Kali Deres</span></li>
                <li  data-id=""><img src="images/content/dealer-18.jpg"width="198"height="139"class="img"/> <span><img src="images/material/hino.png"width="57"height="13"/> Cengkareng</span></li>
                <li ><img src="images/content/dealer-08.jpg"width="199"height="140"class="img"/> <span><img src="images/material/hino.png"width="57"height="13"/> Pluit</span></li>
                <li  data-id="kemayoran"><img src="images/content/dealer.jpg"width="199"height="140"class="img"/> <span><img src="images/material/hino.png"width="57"height="13"/> Kemayoran</span></li>
                <li data-id="tomang"><img src="images/content/dealer-08.jpg"width="198"height="139"class="img"/> <span><img src="images/material/hino.png"width="57"height="13"/> Tomang</span></li>
                <li><img src="images/content/dealer-12.jpg"width="198"height="140"class="img"/> <span><img src="images/material/hino.png"width="57"height="13"/> Kali Deres</span></li>
                <li  data-id=""><img src="images/content/dealer-18.jpg"width="198"height="139"class="img"/> <span><img src="images/material/hino.png"width="57"height="13"/> Cengkareng</span></li>
                <li ><img src="images/content/dealer-08.jpg"width="199"height="140"class="img"/> <span><img src="images/material/hino.png"width="57"height="13"/> Pluit</span></li>
              </ul>
              <form action=""method="get"class="dealer_search">
                <span>Location  : </span>
                <label>Province</label>
                <select name="province">
                  <option value="Show All">Show All</option>
                </select>
                <label>City</label>
                <select name="city">
                  <option value="Show All">Show All</option>
                </select>
                <input name="search"type="submit"value="Submit"/>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end middle -->
  
  <div style="display:none;"> 
    <div id='kemayoran'>
      <div class="dealer_detail">
        <div class="header"> <span class="img_title"> <img src="images/material/hino_white.png"width="75"height="18"/> </span>Kemayoran
        <img src="images/material/btn_close.png"width="23"height="23"class="close"/> </div>
        <div class="sectionLeft">
        	<img src="images/content/dealer.jpg"width="230"height="150"class="big_image"/>
            <ul>
            	<li><img src="images/material/icon_dealerdetail_03.png"width="20"height="20"/>Sales</li>
                <li><img src="images/material/icon_dealerdetail_05.png"width="20"height="20"/>Spare parts</li>
                <li><img src="images/material/icon_dealerdetail_09.png"width="20"height="20"/>Services</li>
                <li><img src="images/material/icon_dealerdetail_10.png"width="20"height="20"/>Main Dealer</li>
                <li><img src="images/material/icon_dealerdetail_13.png"width="20"height="20"/>Branch Dealer</li>
                <li><img src="images/material/icon_dealerdetail_14.png"width="20"height="20"/>Sub Dealer</li>
            </ul>
        </div>
        <div class="sectionRight">
      <table width="370"border="0"cellpadding="5">
            <tr class="bg01">
              <td colspan="3"><strong>PT. Duta Cemerlang motor</strong></td>
            </tr>
            <tr class="bg02">
              <td width="110"><img src="images/material/icon_dealerdetail_09.png"width="20"height="20"style="float:left; margin-right:5px"/> <img src="images/material/icon_dealerdetail_03.png"width="20"height="20"style="float:left; margin-right:5px"/> <img src="images/material/icon_dealerdetail_10.png"width="20"height="20"style="float:left; margin-right:5px"/> <img src="images/material/icon_dealerdetail_05.png"width="20"height="20"style="float:left; margin-right:5px"/>  </td>
              <td width="5"> </td>
              <td>Main Dealer 3s, Main Dealer 3s, Main Dealer 3s, Main Dealer 3s</td>
            </tr>
            <tr class="bg01">
              <td valign="top">Dealer type </td>
              <td width="5"align="center"valign="top">:</td>
              <td><span style="color:#555;">Sales, Service, Spareparts</span></td>
            </tr>
            <tr class="bg02">
              <td valign="top">Alamat</td>
              <td align="center"valign="top">:</td>
              <td><span style="color:#555;">Jl. Raya Kaligawe 33, Semarang - Jateng<br />
                Semarang<br />
                Jawa Tengah </span></td>
            </tr>
            <tr class="bg01">
              <td valign="top">Telepon</td>
              <td align="center"valign="top">:</td>
              <td><span style="color:#555;">(024) 6581214, 6581594</span></td>
            </tr>
            <tr class="bg02">
              <td valign="top">Fax</td>
              <td align="center"valign="top">:</td>
              <td><span style="color:#555;">(024) 6581214, 6581594</span></td>
            </tr>
            <tr class="bg01">
              <td valign="top">Emergency<br />
                Call (24 hrs) </td>
              <td align="center"valign="top">:</td>
              <td><span style="color:#555;">(024) 6581214, 6581594<br /></span></td>
            </tr>
          </table>
        </div>
      </div>
    </div>
    
    
    <div id='tomang'>
      <div class="dealer_detail">
        <div class="header"> <span class="img_title"> <img src="images/material/hino_white.png"width="75"height="18"/> </span>Tomang
        <img src="images/material/btn_close.png"width="23"height="23"class="close"/> </div>
        <div class="sectionLeft">
        	<img src="images/content/dealer.jpg"width="230"height="150"class="big_image"/>
            <ul>
            	<li><img src="images/material/icon_dealerdetail_03.png"width="20"height="20"/>Sales</li>
                <li><img src="images/material/icon_dealerdetail_05.png"width="20"height="20"/>Spare parts</li>
                <li><img src="images/material/icon_dealerdetail_09.png"width="20"height="20"/>Services</li>
                <li><img src="images/material/icon_dealerdetail_10.png"width="20"height="20"/>Main Dealer</li>
                <li><img src="images/material/icon_dealerdetail_13.png"width="20"height="20"/>Branch Dealer</li>
                <li><img src="images/material/icon_dealerdetail_14.png"width="20"height="20"/>Sub Dealer</li>
            </ul>
        </div>
        <div class="sectionRight">
      <table width="370"border="0"cellpadding="5">
            <tr class="bg01">
              <td colspan="3"><strong>PT. Duta Cemerlang motor</strong></td>
            </tr>
            <tr class="bg02">
              <td width="110"><img src="images/material/icon_dealerdetail_09.png"width="20"height="20"style="float:left; margin-right:5px"/> <img src="images/material/icon_dealerdetail_03.png"width="20"height="20"style="float:left; margin-right:5px"/> <img src="images/material/icon_dealerdetail_10.png"width="20"height="20"style="float:left; margin-right:5px"/> <img src="images/material/icon_dealerdetail_05.png"width="20"height="20"style="float:left; margin-right:5px"/>  </td>
              <td width="5"> </td>
              <td>Main Dealer 3s, Main Dealer 3s, Main Dealer 3s, Main Dealer 3s</td>
            </tr>
            <tr class="bg01">
              <td valign="top">Dealer type </td>
              <td width="5"align="center"valign="top">:</td>
              <td><span style="color:#555;">Sales, Service, Spareparts</span></td>
            </tr>
            <tr class="bg02">
              <td valign="top">Alamat</td>
              <td align="center"valign="top">:</td>
              <td><span style="color:#555;">Jl. Raya Kaligawe 33, Semarang - Jateng<br />
                Semarang<br />
                Jawa Tengah </span></td>
            </tr>
            <tr class="bg01">
              <td valign="top">Telepon</td>
              <td align="center"valign="top">:</td>
              <td><span style="color:#555;">(024) 6581214, 6581594</span></td>
            </tr>
            <tr class="bg02">
              <td valign="top">Fax</td>
              <td align="center"valign="top">:</td>
              <td><span style="color:#555;">(024) 6581214, 6581594</span></td>
            </tr>
            <tr class="bg01">
              <td valign="top">Emergency<br />
                Call (24 hrs) </td>
              <td align="center"valign="top">:</td>
              <td><span style="color:#555;">(024) 6581214, 6581594<br /></span></td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
  <?php include('inc_footer.php');?>
